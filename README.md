Helm
=========
Installs helm.

Include role
------------
```yaml
- name: install helm
  src: https://gitlab.com/ansible_roles_v/helm/
  version: main
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - helm
```